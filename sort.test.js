const { SortArr } = require("./sort");

describe("SortArr", () => {
  it("sort odd and even", () => {
    expect(SortArr([4, 2, 5, 7, 1, 6])).toEqual([1, 5, 7, 2, 4, 6]);
    expect(SortArr([25, 40, 14, 91, 31, 22, 49, 13, 6])).toEqual([
      13, 25, 31, 49, 91, 6, 14, 22, 40,
    ]);
  });

  it("empty", () => {
    expect(SortArr([])).toEqual([]);
  });
});
