const TwoSumArr = (numbers = [], target = 0) => {
  for (let index = 0; index < numbers.length; index++) {
    for (let j = 0; j < numbers.length; j++) {
      if (index != j) {
        if (numbers[index] + numbers[j] === target) {
          return true;
        }
      }
    }
  }
  return false;
};

module.exports = { TwoSumArr };
