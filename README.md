# Javascript And Basic Unit Test


## Sum Of Two in Array

### Task: Implement a JavaScript function that, given an array of integers and a target integer, returns whether any two integers in the array sum up to the target number

```
Example:
Input: [1, 3, 5, 7, 16, 4], Target: 8, Output: true.
Input: [1, 3, 5, 7, 16, 4], Target: 12, Output: true.
Input: [1, 3, 5, 7, 16, 4], Target: 18, Output: false.
Input: [12, 17, 14, 11, 19, 8], Target: 20 Output: true
```

## Custom Sort Function
### Task: Write a JavaScript function that sorts an array of numbers in ascending order, but places all odd numbers before even numbers.

```
Example:
Input: [4, 2, 5, 7, 1, 6], Output: [1, 5, 7, 2, 4, 6].
Input: [25, 40, 14, 91, 31, 22, 49, 13, 6], Output: [13, 25, 31, 49, 91, 6, 14, 22, 40].
```

## Largest Number
### Task: Given a list of non-negative integers, arrange them such that they form the largest possible number.

```
Example
Input: [10, 2, 13, 7], Output: "721310".
Input: [21, 36, 8, 45], Output: “8453621”
```

## Sum of All Primes
### Task: Write a JavaScript function that takes a number n and returns the sum of all prime numbers up to and including n.

```
Example:
Input: 10, Output: 17 (since the prime numbers up to 10 are 2, 3, 5, 7).
Input: 59, Output: 440 (since the prime numbers up to 10 are 2, 3, 5, 7….. To 59).
```



### How to start project with run unit test

```
1. npm i 
```

```
2 npm test
```

