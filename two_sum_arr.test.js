const { TwoSumArr } = require("./two_sum_arr");

describe("TwoSumArr", () => {
  it("should return true when two numbers add up to the target", () => {
    expect(TwoSumArr([1, 3, 5, 7, 16, 4], 8)).toBe(true);
    expect(TwoSumArr([12, 17, 14, 11, 19, 8], 20)).toBe(true);
    expect(TwoSumArr([1, 3, 5, 7, 16, 4], 12)).toBe(true);
  });

  it("should return false when no two numbers add up to the target", () => {
    expect(TwoSumArr([1, 3, 5, 7, 16, 4], 18)).toBe(false);
  });
});
