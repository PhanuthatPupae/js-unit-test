const SumPrimes = (number = 0) => {
  let sum = 0;
  for (let index = 0; index <= number; index++) {
    if (isPrime(index)) {
      sum += index;
    }
  }
  return sum;
};

//ref https://www.kroobannok.com/90465
const isPrime = (number = 0) => {
  //skip number 1
  if (number < 2) {
    return false;
  }
  const defaultPrimeNumber = 2;
  for (let index = defaultPrimeNumber; index < number; index++) {
    if (number % index === 0) {
      return false;
    }
  }
  return true;
};

module.exports = { SumPrimes };
