const { TwoSumArr } = require("./two_sum_arr");
const { SortArr } = require("./sort");
const { LargestNumber } = require("./largest_number");
const { SumPrimes } = require("./sum_primes");

// const result = TwoSumArr([1, 3, 5, 7, 16, 4], 12);
// console.log("result===>", result);

// const resultSort = SortArr([4, 2, 5, 7, 1, 6]);
// console.log("result===>", resultSort);

// const resultLargestNumner = LargestNumber([10, 2, 13, 7]);
// console.log(resultLargestNumner);

const resultSumPrimes = SumPrimes(10);
console.log("result", resultSumPrimes);
