const LargestNumber = (numbers = []) => {
  const numberStrList = numbers.map((n) => n.toString());
  const sort = numberStrList.sort((a, b) => {
    return a.concat(b) < b.concat(a) ? 1 : -1;
  });
  return sort.join("");
};
module.exports = { LargestNumber };
