const { LargestNumber } = require("./largest_number");

describe("LargestNumber", () => {
  it("largest number", () => {
    expect(LargestNumber([10, 2, 13, 7])).toEqual("721310");
    expect(LargestNumber([21, 36, 8, 45])).toEqual("8453621");
  });

  it("empty array", () => {
    expect(LargestNumber([])).toEqual("");
  });

  it("zero", () => {
    expect(LargestNumber([0])).toEqual("0");
  });
});
