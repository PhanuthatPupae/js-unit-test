const SortArr = (numbers = []) => {
  const { odd, even } = GetArrOddEven(numbers);
  return [...odd.sort((a, b) => a - b), ...even.sort((a, b) => a - b)];
};

const GetArrOddEven = (numbers = []) => {
  const odd = [];
  const even = [];
  if (numbers.length > 0) {
    for (let index = 0; index < numbers.length; index++) {
      const n = numbers[index];
      if (n % 2 === 0) {
        even.push(n);
      } else {
        odd.push(n);
      }
    }
  }
  return { odd, even };
};

module.exports = { SortArr };
