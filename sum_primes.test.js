const { SumPrimes } = require("./sum_primes");

describe("SumPrimes", () => {
  it("sum of all prime numbers up to and including n", () => {
    expect(SumPrimes(10)).toEqual(17);
    expect(SumPrimes(59)).toEqual(440);
  });

  it("sum of all prime numbers up to and including n n is zero", () => {
    expect(SumPrimes(0)).toEqual(0);
  });
  it("sum of all prime numbers up to and including n n is negative", () => {
    expect(SumPrimes(-2)).toEqual(0);
  });
});
